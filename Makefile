GO ?= go

speedtest:
	$(GO) build $(GOFLAGS) -o $@ ./bin/$@/...

clean:
	$(RM) speedtest

.PHONY: clean speedtest

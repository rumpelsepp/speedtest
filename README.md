The Unofficial Speedtest CLI
============================

The Unofficial Speedtest CLI is a command-line program to test
bandwidth in situations where you don't have access to a full GUI
environment and web browser.

In [2013 I was feeling guilty](http://thehelpfulhacker.net/2013/07/29/giving-something-back/)
about using Open Source software for most of my life without giving
anything back in return.  I decided to create this project to my part
to help the IT community.

A lot of the initial algorithms here are based on different scripts I
found when I was studying how speedtest.net works.  Mainly, @sivel's
[speedtest-cli](https://github.com/sivel/speedtest-cli),
thanks for your work!
